job "[[ .deployment.job_name ]]" {
  datacenters = [[ .deployment.datacenters | toJson ]]
  type = "service"
  group "[[ .deployment.group_name ]]" {
    count = [[ .deployment.node_count ]]
    
    volume "data" {
      type = "host"
      source = "data"
      read_only = false
      per_alloc       = true
    }

    network {
      mode = "bridge"

      port "http" {
        to = 7000
      }
    }
    service {
      name = "[[ .deployment.service_name ]]"
      port     = "http"
      check {
        name     = "HTTP Check"
        type     = "http"
        path     = "/health"
        interval = "5s"
        timeout  = "2s"
      }
      connect {
        sidecar_service {
          proxy {
            local_service_port = 9090
          }
        }
      }
    }
    task "[[ .deployment.task_name ]]" {
      driver = "docker"
      
      volume_mount {
        volume      = "data"
        destination = "[[ .deployment.env.LOG_DIR ]]"
        read_only=false
      }
      
      config {
        image = "[[ .deployment.registry.image ]]:[[ or .deployment.registry.tag .tag]]"
        ports = ["http","grpc"]  
        
        auth {
          server_address = "[[ .deployment.registry.address ]]"
          username = "[[ or .deployment.registry.auth.username .username ]]"
          password = "[[ or .deployment.registry.auth.password .password ]]"
        }    
      }
      env {
        // ENABLE_DEBUGGING = "[[ .deployment.env.ENABLE_DEBUGGING ]]"
        // KAFKA_TOPIC = "[[ .deployment.env.KAFKA_TOPIC ]]"
        ENV = "[[ .deployment.env.ENV ]]"
        HOST = "[[ .deployment.env.HOST ]]"
        API_PORT = "[[ .deployment.env.API_PORT ]]"
        LOG_DIR = "[[ .deployment.env.LOG_DIR ]]"
      }
      
      resources {
        cpu = [[ .deployment.resource.cpu ]]
        memory = [[ .deployment.resource.memory ]]
      }
    }
  }
}
