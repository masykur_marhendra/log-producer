module log-producer

go 1.13

require (
	github.com/gorilla/mux v1.8.0
	github.com/rs/zerolog v1.23.0
	golang.org/x/tools/gopls v0.7.1 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
