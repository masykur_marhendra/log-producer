FROM golang:1.13-alpine as builder
RUN apk update && apk add --no-cache ca-certificates && update-ca-certificates
RUN adduser -D -g '' appuser
WORKDIR $GOPATH/src/gitlab.com/masykur_marhendra/log-producer
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /main


FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /main /main


ARG env
ENV ENVIRONMENT $env
# need to run as root to allow postgres use the private key with 0600 permission
USER appuser
EXPOSE 7000 7000
ENTRYPOINT ["/main"]
