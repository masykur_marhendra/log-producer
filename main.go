package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"log-producer/logger"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/gorilla/mux"
)

var (
	chWork      <-chan struct{}
	chControl   chan struct{}
	wg          sync.WaitGroup
	multiLogger *logger.Logger
	pe          *PurchaseEvent = &PurchaseEvent{
		ChannelTransactionId: "C002210610085838797000000 ",
		OrderId:              "f0179f3a4fe7676236000311151cfd9Sac100000000",
		Msisdn:               "6282113676236",
		ServiceIDB:           "6282113676236",
		LacMsisdn:            "278370",
		CiMsisdn:             "13",
		AreaMsisdn:           "KOTA PONTIANAK",
		OrderType:            "ACT",
		PaymentMethod:        "SELF",
		ReasonCode:           "BALANCE",
		BrandMsisdnA:         "Simpati",
		BrandMsisdnB:         "Simpati",
		TimestampStart:       "2021-06-09 14:32:01.000",
		TimestampEnd:         "2021-06-09 14:32:03.000",
		StatusCode:           "OK01",
		StatusMessage:        "OK",
		Price:                15000,
		BusinessID:           "00031115",
		ProductAppsID:        "FSD",
		ProductFamily:        "Internet",
		ProductValidityValue: "30",
		ProductValidityUOM:   "days",
	}
)

type Command struct {
	// LogPath  string `json:"log_path"`
	Interval int `json:"interval"`
}
type PurchaseEvent struct {
	ChannelTransactionId string `json:"channel_transaction_id"`
	OrderId              string `json:"order_id"`
	Msisdn               string `json:"msisdn"`
	ServiceIDB           string `json:"service_id_b"`
	LacMsisdn            string `json:"lac_msisdn"`
	CiMsisdn             string `json:"ci_msisdn"`
	AreaMsisdn           string `json:"area_msisdn"`
	OrderType            string `json:"order_type"`
	PaymentMethod        string `json:"payment_method"`
	ReasonCode           string `json:"reason_code"`
	BrandMsisdnA         string `json:"brand_msisdn_a"`
	BrandMsisdnB         string `json:"brand_msisdn_b"`
	TimestampStart       string `json:"timestamp_start"`
	TimestampEnd         string `json:"timestamp_end"`
	StatusCode           string `json:"status"`
	StatusMessage        string `json:"status_message"`
	Price                int    `json:"price"`
	BusinessID           string `json:"business_id"`
	ProductAppsID        string `json:"product_apps_id"`
	ProductFamily        string `json:"product_family"`
	ProductValidityValue string `json:"product_validity_value"`
	ProductValidityUOM   string `json:"product_validity_uom"`
	EventTimestamp       string `json:"event_timestamp"`
}

type Response struct {
	MetaResponse Meta `json:"meta"`
}

type Meta struct {
	Code   string `json:"code"`
	Result string `json:"result"`
}

func logging(interval int) {
	for {
		select {
		case <-chWork:
			pe.EventTimestamp = time.Now().String()
			hostname, _ := os.Hostname()
			multiLogger.Info().Str("hostname", hostname).Interface("data", pe).Msg("")
			time.Sleep(time.Duration(interval) * time.Millisecond)
		case _, ok := <-chControl:
			if ok {
				continue
			}
			return
		}
	}
}
func start(interval int) {
	ch := make(chan struct{})
	close(ch)
	chWork = ch

	// chControl
	chControl = make(chan struct{})

	// wg
	wg = sync.WaitGroup{}
	wg.Add(1)

	go logging(interval)
}

func stop() {
	chWork = nil
	close(chControl)
	fmt.Println("stopping go routine")
}

func startStreaming(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var command Command
	json.Unmarshal(reqBody, &command)

	fmt.Println("[INFO] Starting logging activity")

	start(command.Interval)
	res := &Response{
		MetaResponse: Meta{
			Code:   "00",
			Result: "OK",
		},
	}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(res)
}

func stopStreaming(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[INFO] Stopping logging activity")
	stop()
	fmt.Println("[INFO] Logging activity stopped")
	res := &Response{
		MetaResponse: Meta{
			Code:   "00",
			Result: "OK",
		},
	}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(res)
}
func health(w http.ResponseWriter, r *http.Request) {
	res := &Response{
		MetaResponse: Meta{
			Code:   "00",
			Result: "OK",
		},
	}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(res)

}

func handleRequests() {
	r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/dom/events/start", startStreaming).Methods("POST")
	r.HandleFunc("/dom/events/stop", stopStreaming).Methods("POST")
	r.HandleFunc("/health", health).Methods("GET")
	fmt.Println("[INFO] Starting server at port 7000")
	log.Fatal(http.ListenAndServe(":7000", r))
	fmt.Println("[INFO] Server started at port 7000")
}

func configureLog() {
	pwd := os.Getenv("LOG_DIR")
	if pwd == "" {
		pwd = "/tmp/mylogs-default/"
	}
	config := &logger.Config{
		ConsoleLoggingEnabled: true,
		EncodeLogsAsJson:      false,
		FileLoggingEnabled:    true,
		Directory:             pwd,
		Filename:              "dom_log",
		MaxSize:               10240,
		MaxBackups:            10,
		MaxAge:                10,
	}

	multiLogger = logger.Configure(*config)

}
func main() {
	configureLog()
	handleRequests()
}
